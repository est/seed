#!/bin/bash

suite="$1"

if [ -z "$suite" ]; then
  printf "Please pass the suite (buster, bullseye, etc) as the first argument.\n"
  exit 1
fi

printf "Building my-${suite} image\n"
user=$(whoami)
if [ "$user" != "root" ]; then
  printf "You have to be root to create the first base image\n"
  exit 1
fi

apt_server="$2"
apt_proxy="$3"

temp=$(mktemp -d)

# Ensure we don't create a 700 permission root file system in our container.
chmod 755 "$temp"

echo "Running debootstrap"
export LC_ALL=C && debootstrap --variant=minbase --include=apt-utils,less,iputils-ping,iproute2,vim,locales,libterm-readline-gnu-perl,dnsutils,procps "$suite" "$temp" "${apt_server}" 

# Make all servers America/New_York
echo "America/New_York" > "$temp/etc/timezone"
chroot "$temp" /usr/sbin/dpkg-reconfigure --frontend noninteractive tzdata

# Upgrade
echo "deb http://security.debian.org/debian-security ${suite}-security main" > "$temp/etc/apt/sources.list.d/security.list"
echo "deb http://http.us.debian.org/debian/ ${suite}-updates main" > "$temp/etc/apt/sources.list.d/update.list"
if [ -n "$apt_proxy" ]; then
  echo "Acquire::http::proxy \"$apt_proxy\";" > "$temp/etc/apt/apt.conf.d/02proxy"
fi
echo "Upgrading"
chroot "$temp" apt-get update
chroot "$temp" apt-get -y dist-upgrade

echo "Importing into docker"
pwd=$(pwd)
cd "$temp" && tar -c . | docker import - "my-${suite}"
cd "$pwd"
echo "Removing temp directory"
rm -rf "$temp"

