#!/usr/bin/python3

# Manage the inventory, including validation of the yaml files using
# our own validators to detect problems specific to our data.
import os
import sys
import yaml
import socket 
from IPy import IP
from dns import resolver

class SowerInventory():
    VALIDATION_LEVEL_NONE = 0
    VALIDATION_LEVEL_MINIMAL = 5
    VALIDATION_LEVEL_FULL = 10
    validation_level = VALIDATION_LEVEL_MINIMAL 
    live = False
    seed_dir = None
    inventory_dir = None
    # Dict of hosts.yml file.
    hosts_yaml = {} 

    # All host vars keyed by host name.
    host_vars_yaml = {}
     
    # Retreive the path to the active inventory directory.
    def set_inventory_dir(self):
        if self.live == True:
            self.inventory_dir = os.environ.get('SEED_INVENTORY_LIVE_DIR')
        else:
            self.inventory_dir = self.seed_dir + "/inventories/dev"

        # If we have an inventory_dir, make sure it exists.
        if not os.path.exists(self.inventory_dir):
            raise RuntimeError("The live inventory directory set by the SEED_INVENTORY_LIVE_DIR does not exist: {0}".format(self.inventory_dir))

    """
    Return a dictionary containing the hosts yaml configuration.
    """
    def set_hosts(self):
      # Get the path to our hosts file.
      # Load the hosts.yml file.
      file = self.inventory_dir + "/hosts.yml"
      if not os.path.exists(file):
          raise RuntimeError("Unable to find your hosts.yml file. You may need to copy inventories/dev/hosts.yml.default to hosts.yml")
      with open(file) as f:
        self.hosts_yaml = yaml.load(f, Loader=yaml.FullLoader)

    """
    Set the dictionary with the yaml configuration of each host.
    """
    def set_host_vars(self):
      host_vars_dir = self.inventory_dir + "/host_vars"
      for filename in os.listdir(host_vars_dir):
        filepath = host_vars_dir + "/" + filename
        host_name = os.path.splitext(filename)[0]
        if os.path.isfile(filepath):
            with open(filepath) as f:
                self.host_vars_yaml[host_name] = yaml.load(f, Loader=yaml.FullLoader)

    def validate(self):
      if self.validation_level == self.VALIDATION_LEVEL_NONE:
          return True
      self.set_hosts()
      self.set_host_vars()
      seen_ips = []
      seen_data_disks = []
      seen_kvm_names = []
      seen_simplemonitor_labels = []
      seen_postfix_instances = []
      seen_proxysql_hostgroup_ids = []
      # Sometimes we take a mail relay IP out of rotation because it is bouncing. Keep
      # track of those and warn if they are present.
      unused_mail_relay_ips = []
      for host_name in self.host_vars_yaml:
          values = self.host_vars_yaml[host_name]
          # Ensure each host has a member_id variable
          if not "member_id" in values:
              raise RuntimeError("Missing member_id in {0} file".format(host_name))
          # Ensure no IP address is assigned twice and is properly assigned with a mask.
          if "network" in values:
              for network in values['network']:
                  if "networks" in network:
                      for address_block in network['networks']:
                          if 'address' in address_block:
                              address = address_block['address']
                              if address in seen_ips:
                                  raise ValueError("The IP address {0} was found twice. Last found in {1}.".format(address, host_name ))
                              seen_ips.append(address)
                              if not "/" in address:
                                  raise ValueError(f"The IP address: {address} in host {host_name} is not defined with a /mask.")

          # Ensure no kvm name is defined twice.
          if "kvm" in values:
              if values["kvm"]["name"] in seen_kvm_names:
                  raise ValueError("The kvm name {0} was found twice. Last found in {1}.".format(values["kvm"]["name"], host_name ))
              seen_kvm_names.append(values["kvm"]["name"])

              if "data_disks" in values['kvm']:
                  for data_disk in values['kvm']['data_disks']:
                      if data_disk['name'] in seen_data_disks:
                          raise ValueError("The data disk {0} was found twice. Last found in {1}.".format(data_disk['name'], host_name ))
                      seen_data_disks.append(data_disk['name'])

          # Ensure no simple monitor label is defined twice.
          if "simplemonitor" in values:
              for upstream in values["simplemonitor"]:
                  for label in upstream["checks"]:
                      if label in seen_simplemonitor_labels:
                          raise ValueError("The simplemonitor label {0} was found twice. Last found in {1}.".format(label, host_name ))
                  seen_simplemonitor_labels.append(label)

          # Ensure backup hour is properly set.
          if "backup" in values:
              if "hour" in values["backup"]:
                  if not isinstance(values["backup"]["hour"], int):
                      raise ValueError("When defining an hour for backup, don't use quotes to ensure it is an integer. Found {0} in {1}.".format(values["backup"]["hour"], host_name ))
                  hour = int()
                  if values["backup"]["hour"] > 24 or values["backup"]["hour"] < 0:
                      raise ValueError("When defining an hour for backup, it must be a number between 0 and 24. Found {0} in {1}.".format(values["backup"]["hour"], host_name))

          # Ensure no duplicate proxysql hostgroup ids.
          if "proxysql" in values:
              if "hostgroup_id" in values["proxysql"]:
                  if values['proxysql']['hostgroup_id'] in seen_proxysql_hostgroup_ids:
                      raise ValueError(f"The proxysql hostgroup_id {values['proxysql']['hostgroup_id']} was seen twice. Last seen in {host_name}")
                  seen_proxysql_hostgroup_ids.append(values['proxysql']['hostgroup_id'])

          # Ensure proper backup values.
          if "backup" in values:
              if "remotes" in values["backup"]:
                  short_host_name = host_name.split('.')[0]
                  for backup_definition in values['backup']['remotes']:
                      if backup_definition['user'] != f"{short_host_name}-sync":
                        raise ValueError(f"The backup user in {host_name} is {backup_definition['user']} instead of the expected {short_host_name}-sync")

          if self.validation_level == self.VALIDATION_LEVEL_MINIMAL:
              continue

          # Add expensive tests below.
          # Ensure relay IP addresses are configured properly.
          if self.live == True and host_name.startswith("mailrelay") and "postfix_instances" in values:
              # Generate a list of SPF records. We will later query each IP address to
              # ensure it has an SPF record set.
              existing_spf_ips = []
              for answer in resolver.resolve("spf.mayfirst.org", 'A'):
                  existing_spf_ips.append(answer.to_text())

              for instance in values["postfix_instances"]:
                  if instance["name"] in seen_postfix_instances:
                      raise ValueError("The postfix instance name {0} was found twice. Last found in {1}.".format(instance["name"], host_name ))

                  # Gather details about this instance.
                  seen_postfix_instances.append(instance["name"])
                  ip = instance["ip"]
                  instance_type = instance["type"]

                  # All instances relay email using their type based domain
                  # name, e.g. bulk.relay.mayfirst.org, or
                  # priority.relay.mayfirst.org or filtered.relay.mayfirst.org.
                  host_name_pieces = host_name.split('.')
                  relay_host_name = instance_type + '.relay.mayfirst.' + host_name_pieces[2] + '.'

                  # We have to do a reverse lookup of the IP address to check for pointer
                  # records. This takes an IP like 1.2.3.4 and converts it to 
                  # 4.3.2.1.in-addr.arpa. This step helps us ensure that each IP address
                  # has a reverse lookup pointing to their relay_host_name.
                  ipy = IP(ip)
                  ip_reverse_name = ipy.reverseNames()[0]
                  
                  # Generate a list of existing records.
                  existing_pointers = []
                  for answer in resolver.resolve(ip_reverse_name, 'PTR'):
                      existing_pointers.append(answer.to_text())
                  existing_ips = []
                  for answer in resolver.resolve(relay_host_name, 'A'):
                      existing_ips.append(answer.to_text())

                  if len(existing_pointers) > 1:
                      raise RuntimeError("A relaying mail IP address must only have one reverse lookup. Please check {0}".format(ip))
                  if not relay_host_name in existing_pointers:
                      raise RuntimeError("Mail relay IP address {0} does not have reverse lookup set to relay host name {1}.".format(ip, relay_host_name))
                  if ip not in existing_ips:
                      unused_mail_relay_ips.append(ip)
                      # raise RuntimeError("Mail relay IP address {0} does not have an A record assigned to relay host name {1}.".format(ip, relay_host_name))
                  if instance_type != "route" and ip not in existing_spf_ips:
                      raise RuntimeError("Mail relay IP address {0} does not have an A record assigned to spf.mayfirst.org.".format(ip, host_name))

      if unused_mail_relay_ips:
          print("FYI, The following mail relay IPs are not assigned to a host. That might be intentional if this IP has been bouncing. See rbl.ips.yml file in the seed inventory repo. IPs: {0} ".format(",".join(unused_mail_relay_ips)))
