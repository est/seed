#!/usr/bin/python3

# Handles all ip management. 

import os
import sys
import yaml
from IPy import IP
import subprocess
import pprint
from threading import Thread

class PingTest(Thread):
    ip = None
    status = False
    fqdn = None
    reverse_lookup = False

    def __init__(self, ip, reverse_lookup=True):
        Thread.__init__(self)
        self.ip = ip
        self.reverse_lookup = reverse_lookup

    def run(self, ):
        ping_cmd = [ "ping", "-c", "1" ]
        dns_cmd = [ "dig", "+short", "-x" ]
        cmd = ping_cmd.copy()
        cmd.append(self.ip)
        result = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if result.returncode == 0:
            self.status = True 
            if self.reverse_lookup:
                cmd = dns_cmd.copy()
                cmd.append(self.ip)
                # Get the first line of output.
                self.fqdn = subprocess.run(cmd, capture_output=True, text=True).stdout.strip().split("\n")[0]

class SowerIp():
    limit_to_location = None
    inventory = None

    def gatherAssignedIps(self):
      self.inventory.set_host_vars() 

      # Create a simple dict of IPs that are properly assigned so we can easily
      # tell if a pingable IPs is not assigned.
      assigned_ips = {} 
      for host in self.inventory.host_vars_yaml:
        values = self.inventory.host_vars_yaml[host]
        # Ensure no IP address is assigned twice.
        if "network" in values:
          for network in values['network']:
            if "networks" in network:
              for address_block in network['networks']:
                if 'address' in address_block:
                  address = address_block['address'].split("/")[0]
                  #if address.startswith("10.") or address.startswith("192.168."):
                  #    continue
                  assigned_ips[address] = host
      return assigned_ips

    """
    Ensure all IPv4 addresses that respond to a ping are configured on a host.
    and vice versa.
    """
    def reconcile(self):
      print("Testing non-pingable IPs assigned to hosts...")
      pingtests = []
      assigned_ips = self.gatherAssignedIps()
      for address in assigned_ips:
        p = PingTest(address, reverse_lookup=False)
        pingtests.append(p)
        p.start()

      no_ping_report = []
      for p in pingtests:
        p.join()
        if p.status == False:
            no_ping_report.append(p.ip)


      print("Testing for pingable IPs not assigned to hosts... (note: ipv6 not included)")
      # Now go in the opposite direction. What IPs are pinging that are not
      # configured?
      self.inventory.set_hosts() 
      pingtests = []
      # Iterate over each cabinet location.
      for location in self.inventory.hosts_yaml["all"]["vars"]["m_networks"]:
          if self.limit_to_location and location != limit_to_location:
              continue
          # Iterate over each network block in this location.
          for network in self.inventory.hosts_yaml["all"]["vars"]["m_networks"][location]:
              ips = IP(network)
              if ips.version() == 6:
                  continue
              skip = []
              if len(ips) > 1:
                  # If this is a block, we will skip the broadcast,
                  # network, and router address.
                  skip.append(ips.broadcast())
                  skip.append(ips[0])
                  skip.append(ips[1])
              for ip in ips:
                  if ip in skip:
                    continue
                  ip = str(ip)
                  p = PingTest(ip, reverse_lookup=True)
                  pingtests.append(p)
                  p.start()

      no_host_report = {} 
      for p in pingtests:
          p.join()
          if p.status and p.ip not in assigned_ips:
            no_host_report[p.ip] = p.fqdn

      print("---------")
      print("Assigned IP addresses that don't respond to pings:")
      print("---------")
      for ip in no_ping_report:
          print("{0}: {1}".format(ip, assigned_ips[ip]))

      print("")
      print("---------")
      print("Pingable IP addresses not assigned to a host (with reverse lookup):")
      print("---------")
      for ip in no_host_report:
          reverse = "No reverse lookup"
          if no_host_report[ip]:
                reverse = no_host_report[ip]
          print("{0}: {1}".format(ip, reverse))

    """
    Suggest the next available IP address to assign.
    for each range defined.
    """
    def next(self):
        assigned_ips = self.gatherAssignedIps()
        # Iterate over each network block in this location.
        self.inventory.set_hosts() 
        hosts = self.inventory.hosts_yaml
        for location in hosts["all"]["vars"]["m_networks"]:
            if self.limit_to_location and location != self.limit_to_location:
                continue
            for network in hosts["all"]["vars"]["m_networks"][location]:
                ips = IP(network)
                skip = []
                # Note: You can get len(ips) for an ipv6 range - there are too
                # many to count.
                if ips.version() == 6 or len(ips) > 1:
                    # Skip the broadcast address, network address and the gateway 
                    skip.append(ips.broadcast())
                    skip.append(ips[0])
                    skip.append(ips[1])
                for ip in ips:
                    if ip in skip:
                        continue
                    if str(ip) in assigned_ips:
                        continue
                    print("Network: {0}".format(network))
                    print("  Next Free IP: {0}".format(ip))
                    break



    def report(self):
        self.inventory.set_hosts() 
        hosts_yaml = self.inventory.hosts_yaml 
     
        # Check for second argument - to limit by location.

        # Create a dict of all of our networks in the format:
        # locations[location]["stats"][total] => Total IPs in this location
        # locations[location]["stats"][inuse] => Total IPs in this location that are in use
        # locations[location]["stats"][utilization] => Percent of IPs in this location that are in use 
        # locations[location]["networks"][network][total] => Total IPs in this network 
        # locations[location]["networks"][network][inuse] => count of IPs that are in use 
        # locations[location]["networks"][network][utilization] => percent of IPs that are in use 
        # locations[location]["networks"][network][usage][ip] => reverse DNS lookup of the IPs that are in use.
        # totals[total] = Total of all IPs
        # totals[inuse] => Total IPs in use
        # totals[utilization] => Percent of IPs in use 
        report = {
            "locations": {},
            "totals": {}
        }

        networks = hosts_yaml["all"]["vars"]["m_networks"]
        total = 0
        total_inuse = 0
        # Iterate over each cabinet location.
        for location in networks:
            if self.limit_to_location and location != self.limit_to_location:
                continue
            report["locations"][location] = {
                "networks": {},
                "stats": {}
            }
            print("Working on location {0}".format(location))
            location_inuse = 0
            location_total = 0
            # Iterate over each network block in this location.
            for network in networks[location]:
                ips = IP(network)
                # We don't report on ipv6 - too many!
                if ips.version() != 4:
                    continue
                # Keep track of all IPs that are in use and record the FQDN reverse DNS lookup
                usage = {}
                print(".", end="", flush=True)
                # Keep track of stats for this network.
                report["locations"][location]["networks"][network] = {}
                network_total = len(ips)
                report["locations"][location]["networks"][network]["total"] = network_total 
                network_inuse = 0
                broadcast = None
                network_address = None
                if network_total > 1:
                    # If this is a block, then the last IP address is the broadcast
                    # address which should not count and the first two are the network and
                    # gateway addresses which should also not count.
                    broadcast = ips.broadcast()
                    # The first is the network
                    network_address = ips[0]
                    # We don't count the network and broadcast.
                    network_total = network_total - 2
                pingtests = []
                for ip in ips:
                    ip = str(ip)
                    p = PingTest(ip)
                    pingtests.append(p)
                    p.start()
                for p in pingtests:
                    p.join()
                    if p.ip == broadcast or p.ip == network_address:
                        # The broadcast ping test will fail but we are not including
                        # it in our total so it doesn't matter.
                        continue
                    if p.status:
                        network_inuse = network_inuse + 1
                        usage[p.ip] = p.fqdn

                report["locations"][location]["networks"][network]["inuse"] = network_inuse 
                report["locations"][location]["networks"][network]["total"] = network_total
                report["locations"][location]["networks"][network]["utilization"] = "{0:.0%}".format(network_inuse / network_total)
                report["locations"][location]["networks"][network]["usage"] = usage
                location_total = location_total + network_total
                location_inuse = location_inuse + network_inuse

            report["locations"][location]["stats"] = {}
            report["locations"][location]["stats"]["total"] = location_total
            report["locations"][location]["stats"]["inuse"] = location_inuse 
            report["locations"][location]["stats"]["utilization"] = "{0:.0%}".format(location_inuse / location_total)

            total = total + location_total
            total_inuse = total_inuse + location_inuse 


        report["totals"]["total"] = total
        report["totals"]["inuse"] = total_inuse
        report["totals"]["utilization"] = "{0:.0%}".format(total_inuse / total)

        print()
        #pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
        #pp.pprint(report)
        for location in report["locations"]:
            for network in report["locations"][location]["networks"]:
                print("{0}\t{1}\t{2}\t{3}".format(
                    network, 
                    report["locations"][location]["networks"][network]['utilization'],
                    report["locations"][location]["networks"][network]['inuse'],
                    report["locations"][location]["networks"][network]['total']
                ))

        for location in report["locations"]:
            for network in report["locations"][location]["networks"]:
                for ip in report["locations"][location]["networks"][network]["usage"]:
                    print("{0}\t{1}".format(ip, report["locations"][location]["networks"][network]['usage'][ip]))

    
