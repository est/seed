# Generate reports on various aspects of our servers. 
import subprocess
import csv
from tabulate import tabulate
import os

class SowerReport():
    inventory = None
    host = None
    csv_path = None 

    def hardware(self):
        self.inventory.set_hosts()
        headers = [ "host", "cpu", "mem", "no. guests", "no. disks", "size", "internal_disks" ]
        csv_writer = None
        csv_file = None
        if self.csv_path:
            if os.path.exists(self.csv_path):
                raise Exception(f"The CSV file path exists ({self.csv_path}). Please delete it and try again.")
            csv_file = open(self.csv_path, 'w', newline='')
            csv_writer = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(headers)

        screen_data = []
        metal_hosts = list(self.inventory.hosts_yaml['all']['children']['metal']['hosts'].keys())
        telehouse_hosts = list(self.inventory.hosts_yaml['all']['children']['telehouse']['hosts'].keys())
        hosts = []
        for host in metal_hosts:
            # Limit to telehouse
            if host in telehouse_hosts:
                hosts.append(host)
        # Now we add a couple hosts not under ansible so we have a complete picture
        hosts.append('franz.mayfirst.org')
        hosts.append('baubo.mayfirst.org')
        count = len(hosts)
        print(f"Found {count} servers.")
        for host in hosts:
            print(".", end='', flush=True)
            if self.host and self.host != host:
                continue
            cpu_cmd = "cat /proc/cpuinfo | grep ^processor | tail -n1 | cut -d: -f2 | tr -d ' '"
            mem_cmd = "free -g | grep ^Mem: | awk '{print \$2}'"
            kvm_cmd = "kvm-status | grep -v inactive |egrep '^[0-9]' |wc -l"
            disks_cmd = "cat /proc/partitions  | egrep 'nvme[0-9]n1$|sd[a-z]$' |wc -l"
            cmd = f'ssh root@{host} "{cpu_cmd} && {mem_cmd} && {kvm_cmd} && {disks_cmd}"'
            result = subprocess.run(cmd, shell=True, check=True, capture_output=True, text=True)
            answers = result.stdout.split("\n")
            # CPU's are number from 0 so add 1 to the total.
            cpu = int(answers[0]) + 1
            mem = answers[1]
            kvm = answers[2]
            disks = int(answers[3])
            size = "1u"
            internal_disks = "no"
            if disks == 6:
                internal_disks = "yes"
            if disks > 6:
                size = "2u"

            # Special exceptions based on knowledge
            if host == 'linda.mayfirst.org':
                size = '2u'
                internal_disks = 'no'
            elif host == 'baubo.mayfirst.org':
                kvm = 4
            elif host == 'franz.mayfirst.org':
                kvm = 1

            if csv_writer:
                csv_writer.writerow([host, cpu, mem, kvm, disks, size, internal_disks])
            screen_data.append([host, cpu, mem, kvm, disks, size, internal_disks])

        print()
        print(tabulate(screen_data, headers=headers))
        if csv_file:
            csv_file.close()

            
