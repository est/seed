#!/usr/bin/python3

# Handles arin utlization reporting. 

import os
import sys
import yaml
from IPy import IP
import subprocess
import pprint
from threading import Thread
from sowerip import PingTest

class SowerArin():
    limit_to_location = None
    inventory = None
    prompt = False

    def report(self):
        self.inventory.set_hosts() 
        hosts_yaml = self.inventory.hosts_yaml 
     
        # Check for second argument - to limit by location.

        # Create a dict of all of our networks in the format:
        # locations[location]["stats"][total] => Total IPs in this location
        # locations[location]["stats"][inuse] => Total IPs in this location that are in use
        # locations[location]["stats"][utilization] => Percent of IPs in this location that are in use 
        # locations[location]["networks"][network][total] => Total IPs in this network 
        # locations[location]["networks"][network][inuse] => count of IPs that are in use 
        # locations[location]["networks"][network][utilization] => percent of IPs that are in use 
        # locations[location]["networks"][network][usage][ip] => reverse DNS lookup of the IPs that are in use.
        # totals[total] = Total of all IPs
        # totals[inuse] => Total IPs in use
        # totals[utilization] => Percent of IPs in use 
        report = {
            "locations": {},
            "totals": {}
        }

        networks = hosts_yaml["all"]["vars"]["m_networks"]
        total = 0
        total_inuse = 0
        # Iterate over each cabinet location.
        for location in networks:
            if self.limit_to_location and location != self.limit_to_location:
                continue
            report["locations"][location] = {
                "networks": {},
                "stats": {}
            }
            print("Working on location {0}".format(location))
            location_inuse = 0
            location_total = 0
            # Iterate over each network block in this location.
            for network in networks[location]:
                ips = IP(network)
                # We don't report on ipv6 - too many!
                if ips.version() != 4:
                    continue
                # Keep track of all IPs that are in use and record the FQDN reverse DNS lookup
                usage = {}
                print(".", end="", flush=True)
                # Keep track of stats for this network.
                report["locations"][location]["networks"][network] = {}
                network_total = len(ips)
                report["locations"][location]["networks"][network]["total"] = network_total 
                network_inuse = 0
                broadcast = None
                network_address = None
                if network_total > 1:
                    # If this is a block, then the last IP address is the broadcast
                    # address which should not count and the first two are the network and
                    # gateway addresses which should also not count.
                    broadcast = ips.broadcast()
                    # The first is the network
                    network_address = ips[0]
                    # We don't count the network and broadcast.
                    network_total = network_total - 2
                pingtests = []
                for ip in ips:
                    ip = str(ip)
                    p = PingTest(ip)
                    pingtests.append(p)
                    p.start()
                for p in pingtests:
                    p.join()
                    if p.ip == broadcast or p.ip == network_address:
                        # The broadcast ping test will fail but we are not including
                        # it in our total so it doesn't matter.
                        continue
                    if p.status:
                        network_inuse = network_inuse + 1
                        usage[p.ip] = p.fqdn

                report["locations"][location]["networks"][network]["inuse"] = network_inuse 
                report["locations"][location]["networks"][network]["total"] = network_total
                report["locations"][location]["networks"][network]["utilization"] = "{0:.0%}".format(network_inuse / network_total)
                report["locations"][location]["networks"][network]["usage"] = usage
                location_total = location_total + network_total
                location_inuse = location_inuse + network_inuse

            report["locations"][location]["stats"] = {}
            report["locations"][location]["stats"]["total"] = location_total
            report["locations"][location]["stats"]["inuse"] = location_inuse 
            report["locations"][location]["stats"]["utilization"] = "{0:.0%}".format(location_inuse / location_total)

            total = total + location_total
            total_inuse = total_inuse + location_inuse 


        report["totals"]["total"] = total
        report["totals"]["inuse"] = total_inuse
        report["totals"]["utilization"] = "{0:.0%}".format(total_inuse / total)

        print()
        #pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
        #pp.pprint(report)
        for location in report["locations"]:
            for network in report["locations"][location]["networks"]:
                if not self.prompt:
                    print("{0}\t{1}\t{2}\t{3}".format(
                        network, 
                        report["locations"][location]["networks"][network]['utilization'],
                        report["locations"][location]["networks"][network]['inuse'],
                        report["locations"][location]["networks"][network]['total']
                    ))

        for location in report["locations"]:
            for network in report["locations"][location]["networks"]:
                for ip in report["locations"][location]["networks"][network]["usage"]:
                    path = "{0}/ipv4-report/{1}.yml".format(self.inventory.inventory_dir, ip)
                    saved = {}
                    keys = [ "description", "category", "customer", "contact" ]
                    if os.path.exists(path):
                        with open(path) as f:
                            saved = yaml.load(f, Loader=yaml.FullLoader)
                    for key in keys:
                        if not key in saved or saved[key] is None:
                            saved[key] = ""
                    if "customer" in saved:
                        if saved["customer"]:
                            saved["description"] = saved["customer"]
                        del(saved["customer"])
                    if self.prompt:
                        default_description = report["locations"][location]["networks"][network]['usage'][ip]
                        print("{0}: Default description is {1}".format(ip, default_description))
                        for key in keys:
                            if key == "customer" or key == "descriptoon":
                                continue
                            default = saved[key]
                            saved[key] = input("{0} [{1}]: ".format(key, saved[key])) 
                            if not saved[key]:
                                saved[key] = default
                        with open(path, "w") as f:
                            yaml.dump(saved, f)

                    else:
                        category = saved["category"]
                        if "categories" not in report['locations'][location]:
                            report['locations'][location]["categories"] = {}
                        if category not in report['locations'][location]["categories"]:
                            report['locations'][location]["categories"][category] = {}
                        report["locations"][location]["categories"][category][ip] = saved
    
        if not self.prompt:
            seen = []
            for location in report["locations"]:
                for category in report["locations"][location]["categories"]:
                    if category not in seen:
                        print(category)
                        seen.append(category)
                    for ip in report["locations"][location]["categories"][category]:
                        saved = report["locations"][location]["categories"][category][ip]
                        print("{0}\t{1}\t{2}".format(ip, saved["description"], saved["contact"]))

