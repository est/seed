#!/usr/bin/python3

# List servers. This script is helpful to see a list of our servers depending
# on the given criteria. Since it simply outputs the server name it can be
# used in a loop to iterate over a set of servers.

import os
import sys
import yaml
import subprocess

class SowerLs():
    inventory = None
    seed_dir = None
    pattern = None
    display = []
    host_vars_yaml = {}
    separator = ":"

    def ls(self):
        # Validate the inventory.
        self.inventory.validate()
        if self.display:
            # Populate the host vars
            self.host_vars_yaml = self.inventory.host_vars_yaml
        for host in self.get_distinct_hosts():
            if self.display:
                print(self.add_attributes(host))
            else:
                print(host)
        return True

    def get_attribute_value(self, host, attribute, value = ""):
        if not value:
            value = self.host_vars_yaml[host] 

        # We have to distinguish between lists and dicts.
        if isinstance(value, list):
            if not attribute.isnumeric():
                # We are looking for a string index in a list. That won't work.
                # Should we throw an error?
                #print("Non numeric attrbitue against list {0}".format(attribute))
                return "" 
            attribute = int(attribute)
            try:
                return value[attribute]
            except IndexError:
                #print("The numeric index {0} is out of range on host: {1}".format(attribute, host))
                return "" 

        if attribute in value:
            return value[attribute]
        # print("Can't find attribute {0} on host {1}".format(attribute, host))
        return "" 

    def add_attributes(self, host):
        line = host
        for attribute_string in self.display.split(","):
            value = ""
            attribute_list = attribute_string.split(".")
            for attribute_key in attribute_list:
                value = self.get_attribute_value(host, attribute_key, value)
                if not value:
                    break
            line += "{0}{1}".format(self.separator, value)
        return line

    """
    Get a distinct list of all hosts in the hosts.yml file.
    """
    def get_distinct_hosts(self):
        distinct_hosts = [];
        host_list = subprocess.run(['ansible', '--inventory', self.inventory.inventory_dir + '/hosts.yml', '--list-hosts', self.pattern], capture_output=True, text=True)
        first = True
        for line in host_list.stdout.split('\n'):
            # Skip the first line which gives us a count.
            if first:
                first = False
                continue
            host = line.replace(" ","") 
            # Skip empty lines.
            if not host:
                continue
            if host not in distinct_hosts:
                distinct_hosts.append(host)
        return distinct_hosts


        

    
