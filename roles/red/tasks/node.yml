
- name: install node packages
  apt:
    name:
      - php-cli
      # For hexdump - to generate password.
      # Note: in bookworm this moves to bsdextrautils
      - bsdmainutils
      # For mf-transfer
      - python3-requests

- name: install packages for dns servers
  apt:
    name:
      - php-sqlite3
  when: red_profile == "dns"


- name: create etc directory for red
  file:
    state: directory
    path: /usr/local/etc/red

- name: create etc directory for mysql pem file
  file:
    state: directory
    path: /etc/mysql

- name: add pem file for secure mysql connection. 
  copy:
    content: "{{ red_mysql_cert }}"
    dest: /etc/mysql/red-cert.pem

- name: check for red_node.conf 
  stat:
    path: /usr/local/etc/red/red_node.conf
  register: red_node_conf_file

- name: generate red password
  shell: hexdump -e '"%_p"' /dev/urandom | tr -d '.\n:[]"! \\*><}{)`(;|=#-' | tr -d "'" | head -c 25
  register: red_generated_password
  when: red_node_conf_file.stat.exists == False and m_environment == "production"

- name: set red credentials 
  set_fact:
    red_password: "{{ red_generated_password.stdout }}"
    red_username: red-{{ inventory_hostname_short }}
    red_host: red.mayfirst.org
  when: red_node_conf_file.stat.exists == False and red_generated_password is defined and m_environment == "production"

- name: set red password (dev)
  set_fact:
    red_password: rednode
    red_username: rednode
    red_host: red.mayfirst.dev
  when: m_environment != "production"

- name: create red configuration file
  template:
    src: red_node.conf.j2
    dest: /usr/local/etc/red/red_node.conf
    mode: 0600
  when: red_node_conf_file.stat.exists == False

- name: create convenience symlink to red-node-update
  file:
    state: link
    src: /usr/local/share/red/node/sbin/red-node-update
    path: /usr/local/sbin/red-node-update

- name: enable root access to execute red.
  lineinfile:
    path: /root/.ssh/authorized_keys
    regexp: /usr/local/share/red/node/sbin/red-node-update
    line: command="OPENSSL_CONF=/usr/local/share/red/common/etc/openssl.bookworm.cnf /usr/local/share/red/node/sbin/red-node-update 2>&1" {{ red_ssh_public_key }}


- name: add disk usage timers 
  import_tasks: disk-usage.yml
  when: red_profile == "nextcloud" or red_profile == "mailstore" or red_profile == "mysql"

- name: copy systemd files for checking for abandoned nextcloud accounts 
  copy:
    src: systemd/{{ item }}
    dest: /etc/systemd/system/
  loop:                                
    - abandoned-nextcloud-accounts.service
    - abandoned-nextcloud-accounts.timer
  notify:
    - reload systemd   
    - enable abandoned-nextcloud-accounts
    - start abandoned-nextcloud-accounts
  when: red_profile == "nextcloud"

- name: copy systemd files for checking DNS alias files 
  copy:
    src: systemd/{{ item }}
    dest: /etc/systemd/system/
  loop:                                
    - update-alias-records.service
    - update-alias-records.timer
  notify:
    - reload systemd   
    - enable update-alias-records 
    - start update-alias-records 
  when: red_profile == "dns"

- name: create a default mount point in dev mode
  file:
    state: directory
    path: /media/data001
  when: m_environment == "dev"


- name: create /var/lib/red directory for logs and other variable data
  file:
    state: directory
    path: /var/lib/red

- name: create directory for storing shell pubkeys
  file:
    state: directory
    path: /var/lib/red/pubkeys
    recurse: true
  when: red_profile == "shell"
