# The relay servers are the last leg of sending all email messages.

- name: install opendkim and essential packages
  apt:
    name:
      - postfix-pcre

- name: check if postfix multi instance has been initialized
  command: grep multi_instance_wrapper /etc/postfix/main.cf
  register: multi_initialized
  failed_when: multi_initialized.rc > 1

- name: initialize postfix multi instance support
  command: postmulti -e init
  when: multi_initialized.rc == 1

- name: setup multi instances
  command: 
    cmd: postmulti -I postfix-{{ item.name }} -e create
    creates: /etc/postfix-{{ item.name }}
  loop: "{{ postfix_instances }}"

# Note: our template includes multi_instance_enable = yes so we don't
# need to use the multi command to enable them.
- name: replace main.cf for multi sites
  template:
    dest: /etc/postfix-{{ item.name }}/main.cf
    src: multi.main.cf.j2
  loop: "{{ postfix_instances }}"

- name: set outgoing smtp bind ip address in master.cf 
  lineinfile:
    path: /etc/postfix-{{ item.name }}/master.cf
    regexp: ^smtp      unix
    line: smtp      unix  -       -       y       -       -       smtp -o smtp_bind_address={{ item.ip }}
  loop: "{{ postfix_instances }}"

- name: add header checks file to discard spam on filtered servers.
  copy:
    dest: /etc/postfix-{{ item.name }}/header-checks.pcre
    content: '/^X-Spam-Flag:.YES/ DISCARD spam'
  loop: "{{ postfix_instances }}"
  when: "item.type == 'filtered'"

- name: add header checks file to route traffic on route servers. 
  copy:
    dest: /etc/postfix-{{ item.name }}/header-checks.pcre
    content: |
      /^X-Mayfirst-Relay: bulk/ FILTER smtp:bulk.relay.mayfirst.{{ m_tld }}
      /^X-Mayfirst-Relay: priority/ FILTER smtp:priority.relay.mayfirst.{{ m_tld }}
  loop: "{{ postfix_instances }}"
  when: "item.type == 'route'"

- name: add alternative transport configuration if necessary.
  lineinfile:
    path: /etc/postfix-{{ item.name }}/main.cf
    line: transport_maps = hash:/etc/postfix-{{ item.name }}/transport
  loop: "{{ postfix_instances }}"
  when: "item.transport is defined"

- name: add transport file if necessary.
  copy:
    dest: /etc/postfix-{{ item.name }}/transport
    content: '{{ item.transport }}'
  loop: "{{ postfix_instances }}"
  when: "item.transport is defined"
  notify: postmap relay transports

- name: add pre-bounce file.
  copy:
    dest: /etc/postfix-{{ item.name }}/prebounce
    content: ''
    force: no
  loop: "{{ postfix_instances }}"
  notify: postmap prebounce

- name: enable postfix instances 
  command: systemctl enable postfix@postfix-{{ item.name }}
  loop: "{{ postfix_instances }}"


