---
# tasks file for kibana
# Note: we include the elasticsearch apt repos on all hosts because
# we use journalbeat to send all logs to elasticsearch, so we don't
# need to add them here.
- name: install kibana
  apt:
    name: 
      - kibana={{ m_elastic_version }}
        # We need sudo so we can run some commands as the kibana user.
      - sudo
    # The es repos may have been added but not yet refreshed
    update_cache: true
    dpkg_options: force-confdef,force-confnew

- name: Change kibana user password 
  uri:
    url: https://{{m_elasticsearch_host}}:{{m_elasticsearch_port}}/_security/user/kibana_system/_password
    method: POST
    body_format: json
    body: "{ \"password\":\"{{ vault_elasticsearch_kibana_password }}\" }"
    status_code: 200
    user: elastic
    password: "{{vault_elasticsearch_elastic_password}}"
    force_basic_auth: yes
  when: vault_elasticsearch_elastic_password is defined and vault_elasticsearch_kibana_password is defined
  #no_log: True

# If the user is not present, then the registered value's content property will be empty
- name: Check if mayfirst user has been created on elasticsearch server.
  uri:
    url: https://{{m_elasticsearch_host}}:{{m_elasticsearch_port}}/_security/user/mayfirst
    method: GET 
    status_code: [ "200", "404" ]
    user: elastic
    password: "{{vault_elasticsearch_elastic_password}}"
    force_basic_auth: yes
  when: vault_elasticsearch_elastic_password is defined and vault_elasticsearch_mayfirst_password is defined
  register: mayfirst_user_present

- name: Create mayfirst user to be used by admins to login to kibana 
  uri:
    url: https://{{m_elasticsearch_host}}:{{m_elasticsearch_port}}/_security/user/mayfirst
    method: POST
    body_format: json
    body: "{ \"password\":\"{{ vault_elasticsearch_mayfirst_password }}\", \"roles\": [ \"superuser\" ] }"
    user: elastic
    status_code: [ "200" ]
    password: "{{vault_elasticsearch_elastic_password}}"
    force_basic_auth: yes
  when: vault_elasticsearch_elastic_password is defined and vault_elasticsearch_mayfirst_password is defined and mayfirst_user_present.status == 404

# Kibana package sets home directory to /home/kibana but doesn't create that directory
# which causes problems when we run commands as kibana. So let's change it to /var/lib/kibana.
- name: register kibana home directory
  command: echo ~kibana
  register: kibana_home

- name: update kibana home directory
  command: usermod --home /var/lib/kibana kibana
  when: kibana_home.stdout == "/home/kibana"

# Since we create the keystore as the kibana user, ansible will complain
# if we don't create this directory.
- name: create .ansible/tmp with proper permissions
  file:
    path: /var/lib/kibana/.ansible/tmp
    owner: kibana
    recurse: true

# By default the keystore is saved in /etc/kibana, but for security reasons,
# kibana can't write to the directory, so we have to make an exception and run
# the initial command to create the keystore as root, then we change
# permissions.
- name: create kibana keystore
  command: /usr/share/kibana/bin/kibana-keystore --allow-root create
  args:
    creates: /etc/kibana/kibana.keystore
  environment:
    BABEL_CACHE_PATH: /var/lib/kibana/optimize/.babel_register_cache.json

- name: ensure kibana keystore is owned by kibana user
  file:
    path: /etc/kibana/kibana.keystore
    owner: kibana
    group: kibana
    mode: 0600

- name: register kibana keystore users
  command: /usr/share/kibana/bin/kibana-keystore list
  become: yes
  become_user: kibana
  register: list_keystore
  environment:
    BABEL_CACHE_PATH: /var/lib/kibana/optimize/.babel_register_cache.json

- name: set kibana password
  become: yes
  become_user: kibana
  shell: echo {{ vault_elasticsearch_kibana_password }} | /usr/share/kibana/bin/kibana-keystore add elasticsearch.password --stdin
  when: vault_elasticsearch_kibana_password is defined and 'elasticsearch.password' not in list_keystore.stdout_lines

- name: harden kibana configuration
  blockinfile:
    path: /etc/kibana/kibana.yml
    block: |
      server.host: "{{ kibana_bind_ip }}"
      elasticsearch.hosts: ["{{ m_elasticsearch_url }}:{{ m_elasticsearch_port }}"]
      elasticsearch.ssl.certificateAuthorities: [ "/etc/ssl/certs/ca-certificates.crt" ]
      elasticsearch.username: "kibana_system"
      server.ssl.enabled: true
      server.ssl.key: /etc/ssl/mayfirst/{{ kibana_cert_name }}/privkey.pem
      server.ssl.certificate: /etc/ssl/mayfirst/{{ kibana_cert_name }}/fullchain.pem
  notify: restart kibana

- name: ensure kibana is running
  service:
    name: kibana
    state: started
    enabled: yes

