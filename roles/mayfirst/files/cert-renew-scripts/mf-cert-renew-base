#!/bin/bash

# Base renewal trigger for all servers. This script copies the keys into the
# proper location and ensures their permissions are secured.

# The first argument is the name of the cert. Strip out any funny business
name=${1//[^a-z0-9_.-]/}

privkey_src_path="/var/lib/keypusher/${name}/privkey.pem"
fullchain_src_path="/var/lib/keypusher/${name}/fullchain.pem"

dest_path="/etc/ssl/mayfirst/${name}"

privkey_dest_path="${dest_path}/privkey.pem"
fullchain_dest_path="${dest_path}/fullchain.pem"

# Ensure our target directory exists.
mkdir -p "$dest_path"

# Ensure it is protected. We will grant execute access to the group
# because some servers will want to chgrp that directory so a non-priv
# user can traverse the path to read the key.
chmod 0710 "$dest_path"
chown root:root "$dest_path"

# Make sure our cert and key files are present.
if [ ! -f "$privkey_src_path" -o ! -f "$fullchain_src_path" ]; then
  printf "Can't find key or cert file.\n"
  exit 1
fi

# Move certs to the right place.
mv "$privkey_src_path" "$privkey_dest_path"
mv "$fullchain_src_path" "$fullchain_dest_path"

# Ensure key is protected (the ownership may be changed by later scripts)
chown root:root "$fullchain_dest_path"
chown root:root "$privkey_dest_path"
chmod 640 "$privkey_dest_path"

