---
# tasks file for metricbeat

- name: install packages
  apt:
    name:
      # sudo is needed for ansible to "become" a user.
      - sudo

- name: Create metricbeat group
  group:
    name: metricbeat
    system: yes

- name: create metricbeat user
  user:
    name: metricbeat 
    home: /var/lib/metricbeat
    group: metricbeat
    system: yes

- name: ensure metricbeat dirs are fully owned by metricbeat user
  file:
    path: "{{ item }}" 
    state: directory
    owner: metricbeat
    recurse: yes
  with_items:
    - /var/lib/metricbeat
    - /var/log/metricbeat

# There is nothing sensitive in this file.
- name: ensure metricbeat user can read metricbeat.yml file
  file:
    path: /etc/metricbeat/metricbeat.yml
    mode: "0655"

# When using become and become_as we need this tmp directory to
# be created to avoid an ansible warning.
- name: create remote temp directory to avoid ansible complaints
  file:
    state: directory
    path: /var/lib/metricbeat/.ansible/tmp
    owner: metricbeat

- name: create metricbeat keystore
  command: metricbeat keystore create
  args:
    creates: /var/lib/metricbeat/metricbeat.keystore
  become: yes
  become_user: metricbeat

- name: register metricbeat keystore users
  command: metricbeat keystore list
  register: list_keystore
  become: yes
  become_user: metricbeat

- name: set metricbeat_writer password
  shell: echo {{ vault_elasticsearch_metricbeat_writer_password }} | metricbeat keystore add METRICBEAT_WRITER_PWD --stdin
  when: vault_elasticsearch_metricbeat_writer_password is defined and 'METRICBEAT_WRITER_PWD' not in list_keystore.stdout_lines
  become: yes
  become_user: metricbeat

- name: set elasticsearch host
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: '  hosts:'
    line: '  hosts: ["{{ m_elasticsearch_host }}:{{ m_elasticsearch_port }}"]'

- name: set elasticsearch protocol 
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "protocol: "
    line: '  protocol: "https"'

- name: set elasticsearch username 
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "username: "
    line: '  username: "metricbeat_writer"'

- name: set elasticsearch password 
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "password: "
    line: '  password: "${METRICBEAT_WRITER_PWD}"'

# We load templates manually on the elasticsearch host.
- name: disable template loading 
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "setup.template.enabled:"
    line: "setup.template.enabled: false"

# We don't have permission to check the ilm, so avoid
# permission errors.
- name: disable ilm check
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "setup.ilm.check_exists:"
    line: "setup.ilm.check_exists: false"

# We don't have permission to write to the ilm, so avoid
# any permission errors.
- name: disable ilm overwrite 
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "setup.ilm.overwrite:"
    line: "setup.ilm.overwrite: false"

# See https://code.mayfirst.org/mfmt/seed-inventory/-/issues/27
- name: avoid seccomp bug in bookworm.
  lineinfile:
    path: /etc/metricbeat/metricbeat.yml
    regex: "seccomp.enabled: false"
    line: "seccomp.enabled: false"
  when: ansible_distribution_release == "bookworm"
  notify:
    - restart metricbeat

- name: create systemd override directory
  file:
    path: /etc/systemd/system/metricbeat.service.d
    state: directory

- name: copy systemd overide file
  copy:
    src: user.conf
    dest: /etc/systemd/system/metricbeat.service.d/user.conf
  notify:
    - refresh systemd
    - restart metricbeat

- name: ensure metric beat is running
  service:
    name: metricbeat
    state: started
    enabled: true

- name: enable key system metricsets
  blockinfile:
    path: /etc/metricbeat/modules.d/system.yml
    block: |
      - module: system
        period: 15s 
        metricsets:
          - diskio 
          - service
  notify: restart metricbeat

- name: Include metal service tasks 
  import_tasks: service.metal.yml
  when: "'metal' in group_names" 

- name: Include postgres service tasks 
  import_tasks: service.postgresql.yml
  when: "'postgresql' in group_names" 
