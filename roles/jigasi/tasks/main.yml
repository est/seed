# Unfortunately, this still requires quite a bit of manual setup.
#
# To setup a Twilio number to use with jigasi: 
# 1. Go to the Phone Numbers menu
#    in Twilio 
# 2. Click Buy a Number 
# 3. Don't configure the number (yet) 
# 4. Click Programmable Voice -> SIP domains 
# 5. Click the "+" sign to add a new SIP domain.  
# 6. Add a Friendly Name and SIP URI 
# 7. Under Voice Authentication, click the "+" sign next to Credential List
# and create a username and password (use your phone number as the username, 
# without the 1).  
# 8. Under Call Control Configuration: 
#    a. Configure with: Webhooks, TwiML Bins, Functions,
# Studio, Proxy 
#    b. A Call Comes In: TwiML Bin, then click "+" to add a TwiML
#       Bin with the content: 
#         <?xml version="1.0" encoding="UTF-8"?> 
#           <Response> 
#             <Dial answerOnBridge="true"> 
#               <Sip> sip:{{ phone_number }}@{{ SIP URI }}.sip.us1.twilio.com </Sip> 
#             </Dial> 
#           </Response> 
# 9. Under SIP Registration, click to Enable 
# 10. Under SIP Registration Authentication, choose the same
#     Credentials List you created above 
# 11. Save the SIP domain.  
# 12. Go back to the Numbers screen and open your phone number 
# 13. Go to the "A Call Comes in" section 
#     a. Choose "TwiML Bin" 
#     b. Select the same TwiML Bin you created above.

# POST Ansible setup steps
#
# Wah. This is so annoying. The XMPP password is defined by the installer, but
# frustratingly not provided in `jigasi/sip-communicator.properties`. To make
# matters more ridiculous, it's defined in this file base64 encoded, but in the
# spot we need it, it has to be plain text. So there's no way we can complete
# this automatically. Sheesh.
#
# Here are the manual instructions: 
# 1. Edit the file: /etc/jitsi/jigasi/sip-communicator.properties 
# 2. Find the setting: net.java.sip.communicator.impl.protocol.sip.acc1403273890647.PASSWORD 
# 3. Echo the value to the `base64 -d` command to decode the password 
# 4. Find the entry: org.jitsi.jigasi.xmpp.acc.PASS= 
# 5. Uncomment it and set it to the decoded value of the XMPP password.

- name: Set debconf options for jigasi
  debconf:
    name: "{{ item.name }}"
    question: "{{ item.question }}"
    value: "{{ item.value }}"
    vtype: "{{ item.vtype }}"
  loop:
    - name: jigasi
      question: jigasi/sip-account
      value: "{{ jigasi_sip_login }}"
      vtype: string
    - name: jigasi
      question: jitsi-videobridge/jvb-hostname
      value: "{{ jitsimeet_domain }}"
      vtype: string

- name: Set debconf options that involve a password
  debconf:
    name: jigasi
    question: jigasi/sip-password
    value: "{{ vault_jigasi_sip_password }}"
    vtype: password
  when: vault_jigasi_sip_password is defined 

- name: add jitsi meet apt gpg key
  copy:
    dest: /etc/apt/trusted.gpg.d/jitsi-archive-keyring.gpg
    src: jitsi-archive-keyring.gpg

- name: add jitsimeet apt repo
  copy:
    dest: /etc/apt/sources.list.d/jitsi-stable.list
    content: deb https://download.jitsi.org stable/

# NOTE: don't install jigasi unless the sip password is defined,
# otherwise it won't be properly configured with the right passwords.
- name: install jigasi
  apt:
    name:
      - jigasi
  when: vault_jigasi_sip_password is defined

- name: install dependencies
  apt:
    name:
      - python3
      - python3-bottle
      - letsencrypt

# The XMPP server is setup with a snake oil certificate. It's on the same host
# as jigasi so we just ingore cert warnings. Probably should fix the cert instead...
- name: ignore cert warnings when connecting to XMPP
  lineinfile:
    path: /etc/jitsi/jigasi/sip-communicator.properties
    regexp: net.java.sip.communicator.service.gui.ALWAYS_TRUST_MODE_ENABLED=true
    line: net.java.sip.communicator.service.gui.ALWAYS_TRUST_MODE_ENABLED=true

# The jigasi installer doesn't properly set things up -  we need to
# authenticate to the XMPP server - 
- name: uncomment and set xmpp login 
  lineinfile:
    path: /etc/jitsi/jigasi/sip-communicator.properties
    regexp: org.jitsi.jigasi.xmpp.acc.USER_ID
    line: org.jitsi.jigasi.xmpp.acc.USER_ID=jigasi@auth.{{ jitsimeet_domain }}

- name: turn off anon login 
  lineinfile:
    path: /etc/jitsi/jigasi/sip-communicator.properties
    regexp: org.jitsi.jigasi.xmpp.acc.ANONYMOUS_AUTH=false 
    line: org.jitsi.jigasi.xmpp.acc.ANONYMOUS_AUTH=false 

# By default, jigasi looks for the Sip header: Jigasi-Conference-Room, but twilio will only send
# headers that start with X, so we set it here.
- name: change sip invite header
  lineinfile:
    path: /etc/jitsi/jigasi/sip-communicator.properties
    line: net.java.sip.communicator.impl.protocol.sip.acc1403273890647.JITSI_MEET_ROOM_HEADER_NAME=X-Room-Name

- name: install nginx http template  used for letsencrypt
  template:
    src: conference-mapper-http.conf.j2
    dest: /etc/nginx/sites-available/conference-mapper-http.conf

- name: create symlink for nginx http template
  file:
    state: link
    src: /etc/nginx/sites-available/conference-mapper-http.conf
    dest: /etc/nginx/sites-enabled/conference-mapper-http.conf
  notify:
    - reload nginx

# Generate letsencrypt file. FIXME: this requires two runs to work.
- name: generate lets encrypt file
  command: certbot certonly --webroot --webroot-path /var/www/html --preferred-challenges http -d conference.mapper.{{ jitsimeet_domain }} --non-interactive --email ssl@mayfirst.org --agree-tos
  args:
    creates: /etc/letsencrypt/live/conference.mapper.{{ jitsimeet_domain }}/fullchain.pem

- name: install nginx https template 
  template:
    src: conference-mapper-https.conf.j2
    dest: /etc/nginx/sites-available/conference-mapper-https.conf
  notify:
    - reload nginx

- name: create symlink for nginx https template
  file:
    state: link
    src: /etc/nginx/sites-available/conference-mapper-https.conf
    dest: /etc/nginx/sites-enabled/conference-mapper-https.conf
  notify:
    - reload nginx

- name: create conference mapper user
  user:
    name: jitsimeet-conference-mapper 
    home: /var/lib/jitsimeet-conference-mapper
    system: yes

- name: install jitsimeet-conference-mapper from git
  git:
    repo: "{{ jigasi_jitsimeet_conference_mapper_git_repo }}"
    dest: /usr/local/src/jitsimeet-conference-mapper
    version: "{{ jigasi_jitsimeet_conference_mapper_version }}"

- name: create symlink to jitsimeet-conference-mapper executable
  file:
    state: link
    src: /usr/local/src/jitsimeet-conference-mapper/jitsimeet-conference-mapper.py
    dest: /usr/local/bin/jitsimeet-conference-mapper

- name: copy jitsimeet conference mapper systemd file
  copy:
    remote_src: yes
    src: /usr/local/src/jitsimeet-conference-mapper/jitsimeet-conference-mapper.service
    dest: /etc/systemd/system/
    force: no

- name: set jitsi meet domain in systemd file.
  lineinfile:
    path: /etc/systemd/system/jitsimeet-conference-mapper.service
    regexp: Environment=JITSIMEET_DOMAIN=
    line: Environment=JITSIMEET_DOMAIN={{ jitsimeet_domain }}
  notify:
    - reload systemd
    - enable jitsimeet-conference-mapper 
    - start jitsimeet-conference-mapper 

- name: set jitsi meet dial in numbers in systemd file.
  lineinfile:
    path: /etc/systemd/system/jitsimeet-conference-mapper.service
    regexp: Environment=DIALIN_NUMBERS=
    line: Environment=DIALIN_NUMBERS={{ jigasi_dialin_numbers }}
  notify:
    - reload systemd
    - enable jitsimeet-conference-mapper 
    - start jitsimeet-conference-mapper 

- name: set jitsi meet port in systemd file.
  lineinfile:
    path: /etc/systemd/system/jitsimeet-conference-mapper.service
    regexp: Environment=PORT=
    line: Environment=PORT=8181
  notify:
    - reload systemd
    - enable jitsimeet-conference-mapper 
    - start jitsimeet-conference-mapper 

- name: set twilio account in systemd file.
  lineinfile:
    path: /etc/systemd/system/jitsimeet-conference-mapper.service
    regexp: Environment=SIP_LOGIN= 
    line: Environment=SIP_LOGIN={{ jigasi_sip_login }} 
  notify:
    - reload systemd
    - enable jitsimeet-conference-mapper 
    - start jitsimeet-conference-mapper 

