#!/usr/bin/python3

# pylint: disable=missing-function-docstring,missing-module-docstring,invalid-name

# This script is called when a certificate has been renewed. It
# copies the cert and key to all relevant hosts and runs a script on
# each host to reload any services that need to be reloaded.
#
# The script is called by both certbot for letsencrypt certs and also
# by our own mfca scripts.
#
# The script requires a path to the directory containing the
# certificates. It can be provided either via the environment
# variable $RENEWED_LINEAGE or as the first positional argument (e.g.
# /etc/letsencrypt/live/domain.com or /etc/ssl/mfca/domain.com). You
# can optionally precede the first positional argument with --config
# /path/to/file, which is the path to the configuration file to use
# to lookup the hosts to distribute the certificate to.

import os
import subprocess
import sys
import argparse
import sqlite3
import hashlib
import yaml


# Define helper functions.
def ssh_command(host, command):
    args = [
        "/usr/bin/ssh",
        "-o",
        "StrictHostKeyChecking=accept-new",
        "-l",
        "keypusher",
        host
    ] + command
    return subprocess.run(args, check=True)

def copy_file(host, local_path, remote_path):
    args = [
        "/usr/bin/rsync",
        "--copy-links",
        "-e",
        "ssh -l keypusher -o StrictHostKeyChecking=accept-new",
        local_path,
        host + ":" + remote_path
    ]
    return subprocess.run(args, check=True)

def hash_str(data):
    hash_obj = hashlib.md5()
    hash_obj.update(data.encode())
    return hash_obj.hexdigest()

def file_is_in_sync(server, path, name):
    sql = "SELECT hash FROM file_sync WHERE name = ?"
    db_cursor.execute(sql, (f"{server}-{name}",))
    record = db_cursor.fetchone()
    if not record:
        return False

    with open(path, 'r', encoding='utf-8') as file_pointer:
        cert = file_pointer.read()
    if hash_str(cert) != record[0]:
        return False
    return True

def update_file_sync_status(server, path, name):
    with open(path, 'r', encoding='utf-8') as file_pointer:
        cert = file_pointer.read()
    sql = "INSERT OR REPLACE INTO file_sync (name, hash) VALUES(?, ?)"
    db_cursor.execute(sql, (f"{server}-{name}", hash_str(cert)))
    db.commit()

def main():
    # Begin main program logic.
    data = []
    for cert_file in cert_files:
        if os.path.exists(cert_file):
            with open(cert_file, encoding='utf-8') as f:
                certs = yaml.load(f, Loader=yaml.Loader)
                if certs:
                    data += certs

    # Iterate over all the certs we know about.
    for item in data:
        if all_certs:
            # We will operate on all keys in our configuration files.
            name = item["name"]
            # We have to calculate the base for this item because we
            # are operating all all of the items..
            if item['ca'] == 'mfca':
                base = f"/etc/ssl/mfca/{item['name']}"
            elif item['ca'] == 'letsencrypt':
                base = f"/etc/letsencrypt/live/{item['name']}"
        else:
            # We can use the base and name passed in by the user.
            name = given_name
            base = given_base

        if name == item["name"]:
            if send_to_server:
                distribute_hosts = {
                    send_to_server: [
                        "/usr/local/sbin/mf-cert-renew-base"
                    ]
                }
            else:
                distribute_hosts = item.get("distribute_hosts", {})
                if distribute_hosts is None:
                    distribute_hosts = {}
                if "distribute_hosts_file" in item:
                    distribute_hosts_file = item["distribute_hosts_file"]
                    if not os.path.exists(distribute_hosts_file):
                        err = ("Failed to find distribute_hosts_file: "
                            f"{distribute_hosts_file}.")
                        raise RuntimeError(err)
                    with open(distribute_hosts_file, encoding='utf-8') as f:
                        extra_hosts = yaml.load(f, Loader=yaml.Loader)
                        if extra_hosts:
                            distribute_hosts.update(extra_hosts)

            # Check for key
            key_path = base + "/privkey.pem"
            if not os.path.isfile(key_path):
                print(f"Failed to find key path {key_path} for {name}")
                sys.exit(1)

            cert_path = base + "/fullchain.pem"
            if not os.path.isfile(cert_path):
                print(f"Failed to find cert path {key_path} for {name}")
                sys.exit(1)

            for host in distribute_hosts:
                if file_is_in_sync(host, cert_path, name) and not force:
                    # We are already in sync, don't re-distribute it.
                    continue
                if not bootstrap:
                    # Ensure target directory exists
                    proc = ssh_command(host, ["mkdir", "-p", name])
                    if proc.returncode != 0:
                        print(f"Failed to mkdir on {host} with name {name}")
                        sys.exit(1)

                    # Copy both files.
                    proc = copy_file(host, key_path, name + "/privkey.pem")
                    if proc.returncode != 0:
                        print(f"Failed to copy file {key_path} on {host}")
                        sys.exit(1)

                    proc = copy_file(host, cert_path, name + "/fullchain.pem")
                    if proc.returncode != 0:
                        print(f"Failed to copy file {cert_path} on {host}")
                        sys.exit(1)

                    # Run renew hooks
                    for renew_hook in distribute_hosts[host]:
                        if copy_only:
                            if renew_hook != "/usr/local/sbin/mf-cert-renew-base":
                                continue
                        # We modify the hook - prepend sudo and append the name of the cert.
                        cmd = ["/usr/bin/sudo", renew_hook, name]
                        proc = ssh_command(host, cmd)
                        if proc.returncode != 0:
                            print("Failed to run renew command "
                                f"{renew_hook} on {host} for {name}")
                            sys.exit(1)

                update_file_sync_status(host, cert_path, name)

# Main program logic.

# Setup paths to the sqlite database that keeps track of which servers have
# already received which keys and certs - this allows us to easily
# re-distributre keys and certs to any servers that are missing a key/cert
# without needlessly copying files that have already been copied.
sqlite_dir = "/var/lib/mf-cert-distribute"
sqlite_file = f"{sqlite_dir}/dist.db"

if not os.path.exists(sqlite_dir):
    os.mkdir(sqlite_dir)

init_db = False
if not os.path.exists(sqlite_file):
    init_db = True

db = sqlite3.connect(sqlite_file)
db_cursor = db.cursor()

if init_db:
    # Initialize the database.
    db_cursor.execute("""
        CREATE TABLE IF NOT EXISTS file_sync (
            name TEXT PRIMARY KEY NOT NULL,
            hash TEXT NOT NULL
        );
    """)


# Set default cert configuration files
cert_files = [
    "/etc/ssl/ansible.certs.yml",
    "/etc/ssl/red.certs.yml",
]

force = False
copy_only = False
all_certs = False
bootstrap = False
send_to_server = None

# Either set the base dir via the environment variable (like the Lets Encrypt
# certbot) or set it via args.
given_base = os.getenv('RENEWED_LINEAGE', default=None)
if not given_base:
    parser = argparse.ArgumentParser(
        description=("Distribute x509 keys and certificates to the servers "
            "that need them.")
    )
    parser.add_argument(
        '--config',
        help=("Use an alternative yaml file for certificate configuration. "
            "By default use /etc/ssl/{ansible,red}.certs.yml")
    )
    parser.add_argument(
        '--force',
        help=("Distribute certs even if we think we have already distributed "
            "this cert to the configured servers. Default to no."),
        action='store_true'
    )
    parser.add_argument(
        '--copy-only',
        help=("Only copy the certs, do not run any of the extra scripts to "
            "restart services. Defaults to no."),
        action='store_true'
    )
    parser.add_argument(
        '--bootstrap',
        help=("Only add cert(s) to the file sync database, don't copy anything. "
            "Useful when initializing to record what has already been copied. "),
        action='store_true'
    )
    parser.add_argument(
        '--send-to-server',
        help=("Send certs to the specified server instead of the servers specified in "
            "the certs configuration file.")
    )
    parser.add_argument(
        '--all-certs',
        help="Operate on all certs in the configuration file, instead of passing a path.",
        action='store_true'
    )
    parser.add_argument('path',
        nargs='?',
        help=("Path to the base directory of the cert, e.g. "
            "/etc/letsencrypt/live/site1234, alternatively set this value using "
            "the RENEWED_LINEAGE environment variable. Alternatively, pass --all "
            "and leave off the path.")
    )
    parser.set_defaults(
        config='',
        force=force,
        copy_only=copy_only,
        path='',
        bootstrap=bootstrap,
        send_to_server = send_to_server,
        all_certs = all_certs
    )
    cargs = parser.parse_args()

    force = cargs.force
    copy_only = cargs.copy_only
    bootstrap = cargs.bootstrap
    chosen_cert_file = cargs.config
    given_base = cargs.path
    send_to_server = cargs.send_to_server
    all_certs = cargs.all_certs

    if chosen_cert_file:
        if not os.path.exists(chosen_cert_file):
            raise RuntimeError(f"Failed to find specified cert file: {chosen_cert_file}")
        # Redefine cert_files.
        cert_files = [chosen_cert_file]

    if given_base and all_certs:
        raise RuntimeError("You cannot pass --all-certs and a path to a certificate.")

    if bootstrap and (force or copy_only):
        raise RuntimeError("The --bootstrap argument doesn't make sense with --force or -copy-only.")

if all_certs:
    given_base = None
    given_name = None
else:
    if not os.path.isdir(given_base):
        print(f"Can't find path directory {given_base}.")
        sys.exit(1)

    # Strip trailing slash (if any) otherwise basename will return ''
    given_base = given_base.rstrip("/")
    given_name = os.path.basename(given_base)

main()
