- name: install packages
  apt:
    name:
      - python3-requests
      - python3-boto3
      - python3-colorlog
      - python3-arrow

- name: create configuration directory
  file:
    path: /etc/simplemonitor
    state: directory

- name: add main configuration file
  template:
    src: monitor.ini.j2
    dest: /etc/simplemonitor/monitor.ini
  notify: restart simplemonitor

- name: add file documenting servers to monitor
  template:
    src: monitors.ini.j2
    dest: /etc/simplemonitor/monitors.ini
  notify: restart simplemonitor

# This is temporary while we wait for this to be merged:
# https://github.com/jamesoff/simplemonitor/pull/1354
- name: install simplemonitor from src.
  git:
    repo: "{{ simplemonitor_repo }}" 
    dest: /usr/local/src/simplemonitor
    version: "{{ simplemonitor_version }}"
  notify: restart simplemonitor

- name: install simplemonitor via pip
  pip:
    name: simplemonitor

# Temporary - see above.
- name: remove simplemonitor code directory so it can be symlinked later
  file:
    state: absent 
    path: /usr/local/lib/python3.9/dist-packages/simplemonitor

- name: clean up old symlink
  file:
    state: absent
    path: /usr/local/bin/monitor.py

- name: drop simplemonitor link to more recent code
  file:
    state: link
    force: true
    path: /usr/local/lib/python3.9/dist-packages/simplemonitor
    src: /usr/local/src/simplemonitor/simplemonitor

- name: add systemd service file
  copy:
    src: simplemonitor.service
    dest: /etc/systemd/system/simplemonitor.service

# If the user is not present, then the registered value's content property will be empty
- name: Check if simplemonitor user has been created on elasticsearch server.
  uri:
    url: https://{{m_elasticsearch_host}}:{{m_elasticsearch_port}}/_security/user/simplemonitor
    method: GET
    status_code: [ "200", "404" ]
    user: elastic
    password: "{{vault_elasticsearch_elastic_password}}"
    force_basic_auth: yes
  when: vault_elasticsearch_elastic_password is defined and vault_elasticsearch_simplemonitor_password is defined
  register: simplemonitor_user_present

- name: Create simplemonitor user on elastic search server.
  uri:
    url: https://{{m_elasticsearch_host}}:{{m_elasticsearch_port}}/_security/user/simplemonitor
    method: POST
    body_format: json
    body: "{ \"password\":\"{{ vault_elasticsearch_simplemonitor_password }}\", \"roles\": [ \"simplemonitor\" ] }"
    user: elastic
    status_code: [ "200" ]
    password: "{{vault_elasticsearch_elastic_password}}"
    force_basic_auth: yes
  when: vault_elasticsearch_elastic_password is defined and vault_elasticsearch_simplemonitor_password is defined and simplemonitor_user_present.status == 404

- name: create elastic config file for simplemonitor
  template:
    src: elastic.yml.j2
    dest: /etc/simplemonitor/elastic.yml
  when: vault_elasticsearch_simplemonitor_password is defined

- name: add elastic search file
  copy:
    src: simple_elastic_search.py
    dest: /usr/local/sbin/simple-elastic-search
    mode: 0755
  when: "'alert' in group_names"

- name: add elastic pigeon search file
  copy:
    src: simple_elastic_pigeon_search.py
    dest: /usr/local/sbin/simple-elastic-pigeon-search
    mode: 0755
  when: "'alert' in group_names"

- name: add elastic heath check 
  copy:
    src: simple_elastic_health_check.py 
    dest: /usr/local/sbin/simple-elastic-health-check
    mode: 0755
  when: "'alert' in group_names"

- name: add elastic heartbeat check 
  copy:
    src: simple_pigeon_heartbeat_check.py 
    dest: /usr/local/sbin/simple-pigeon-heartbeat-check
    mode: 0755
  when: "'alert' in group_names"

- name: add file with all hosts that should be monitored 
  template:
    src: hosts.yml.j2
    dest: /etc/simplemonitor/hosts.yml

- name: start and enable the service
  systemd:
    name: simplemonitor.service
    daemon_reload: true
    enabled: true
    state: started

- name: add script to ensure simplemonitor is running
  copy:
    src: watch-the-watcher
    dest: /usr/local/sbin/watch-the-watcher
    mode: 0755

- name: add watch the watcher service files
  copy:
    src: "{{ item }}"
    dest: /etc/systemd/system/
  with_items:
    - watch-the-watcher.service
    - watch-the-watcher.timer

- name: enable the watch the watcher timer
  systemd:
    name: watch-the-watcher.timer
    daemon_reload: true
    enabled: true
    state: started 
