import unittest
import greenhouse 
import os
import random, string
from time import sleep
import imaplib

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 
    # List of item ids we have to cleanup
    itemIds = {} 
    redAdminUser = 'admin'
    redAdminPass = 'admin23'
    testEmail = "admin@myorg.dev"
    testDomain = "myorg.dev"
    testCanonicalEmail = "admin@mail.mayfirst.dev"
    testUser = "admin"
    testPass = "admin23"


    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.redUrl = "https://members.mayfirst.{0}/api.php".format(os.environ.get('GREENHOUSE_TLD', 'dev'))
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass
        self.setupControlPanelItems()

    def tearDown(self):
        self.deleteControlPanelItems()
        return 

    def deleteControlPanelItems(self):
        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertTrue(self.plant.redApi('item', 'delete', setProps, whereProps))


    # Add dns, email address via the control panel API.
    def setupControlPanelItems(self):
        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testDomain,
           "dns_type": "mx",
           "dns_server_name": "a.mx.mayfirst.dev",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
           "dns_mx_verified": "1",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testDomain,
           "dns_type": "dkim",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 38,
           "hosting_order_id": 1,
           "mailbox_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 38

        setProps = {
           "service_id": 2,
           "hosting_order_id": 1,
           "email_address": self.testEmail,
           "email_address_recipient": "{0}@mail.mayfirst.dev".format(self.testUser)
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 2

        # Sleep long enough to ensure all postfix databases are reloaded
        #print("Waiting 30 seconds for postfix databases to reload")
        #sleep(30)
        


    # Login with the provied username and password, search for the message with the given
    # subject and then ensure the provided headers are set, asserting each step of the way
    # so we can clearly see where we failed.
    def imapLoginAndFindMessage(self, username, password, subject = None, headers = {}):
        try:
            self.assertTrue(self.plant.emailImapLogin(username, password), "Log in with username '{0}'.".format(username))
        except imaplib.IMAP4.error:
            self.assertTrue(False, "Logged in with username '{0}'.".format(username))
            return

        if subject:
            self.assertTrue(self.plant.emailImapSelectMailbox(), "Select mailbox'.")
            self.assertTrue(self.plant.emailImapFindMessage(subject), "Find message with subject {0}.".format(subject))
        if headers:
            for header in headers:
                self.assertTrue(self.plant.emailImapMatchHeader(header), "Find message with headers {0}.".format(header))
        self.plant.emailImapClose()

    """
    Test to ensure we can create a user with an email box and send ourselves an email
    message that will be properly delivered to our mailbox. This tests multiple components
    together (nsauth and nscache, ldap, postfix, dovecot, etc.)
    """
    def testSendRoundtripEmail(self):
        username = self.testUser 
        password = self.testPass 
        email_domain_name = self.testDomain
        email_local = self.testUser 
        email_address = email_local + '@' + email_domain_name
        email_subject = "Hi Friend: {0}".format(random.random())

        self.assertTrue(self.plant.emailSend(username, password, email_address, email_address, email_subject), "Send email")
        print("Sending to {0}".format(email_address))
        # Sleep to give time for the message to be delivered.
        sleep(5)

        # Try to find the message. And ensure it has headers indicating it has
        # been virus scanned and spam scanned. Note no dkim check because we
        # don't sign internal emails.
        headers = [
                ( "X-Virus-Scanned", "ClamAV using ClamSMTP"),
                ('X-Spam-Status', 'No, score=')
        ]
        self.imapLoginAndFindMessage(username, password, subject = email_subject, headers = headers)

        email_subject = "Hi Friend Canonical: {0}".format(random.random())
        print("Sending to {0}".format(self.testCanonicalEmail))
        self.assertTrue(self.plant.emailSend(username, password, self.testCanonicalEmail, email_address, email_subject), "Send canonical email")
        # Sleep to give time for the message to be delivered.
        sleep(5)

        # Try to find the message. 
        self.imapLoginAndFindMessage(username, password, subject = email_subject)
