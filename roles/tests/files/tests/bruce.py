"""
These tests ensure that we are properly banning IP addresses that try to
dictionary attacks us.

We are specifically testing the bruce code. It involves a lot of pieces:

 * On this test server we are running `bruce-banner` in debug mode. It won't
 actually ban IPs, instead it logs to journald (and we check that log in the
 tests)

 * On monitor001 we are running bruce-elastic - which queries the elasticsearch
 database and passes on bans to `bruce-banner`

 * On mailcf we are running journalbeat - which sends all postfix and dovecot
 logs to log001 (our elasticsearch server)

"""

import unittest
import greenhouse 
import os
import random, string
from time import sleep
import socket
import imaplib
import requests
import subprocess
import pexpect

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 
    # List of item ids we have to cleanup
    itemIds = {} 
    redAdminUser = 'admin'
    redAdminPass = 'admin23'
    testEmail = "admin@myorg.dev"
    testDomain = "myorg.dev"
    testCanonicalEmail = "admin@mail.mayfirst.dev"
    testUser = "admin"
    testPass = "admin23"


    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.redUrl = "https://members.mayfirst.{0}/api.php".format(os.environ.get('GREENHOUSE_TLD', 'dev'))
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass
        self.setupControlPanelItems()

    def tearDown(self):
        self.deleteControlPanelItems()
        # Purge the journald log
        subprocess.run(["journalctl", "--rotate" ])
        subprocess.run(["journalctl", "--flush" ])
        subprocess.run(["journalctl", "--vacuum-time=1s" ])
        return 

    def deleteControlPanelItems(self):
        for itemId in self.itemIds:
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertTrue(self.plant.redApi('item', 'delete', setProps, whereProps))


    # Add dns, email address via the control panel API.
    def setupControlPanelItems(self):
        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testDomain,
           "dns_type": "mx",
           "dns_server_name": "a.mx.mayfirst.dev",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testDomain,
           "dns_type": "dkim",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 38,
           "hosting_order_id": 1,
           "mailbox_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 38

        setProps = {
           "service_id": 2,
           "hosting_order_id": 1,
           "email_address": self.testEmail,
           "email_address_recipient": "{0}@mail.mayfirst.dev".format(self.testUser)
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 2

        # Sleep long enough to ensure all postfix databases are reloaded
        #print("Waiting 30 seconds for postfix databases to reload")
        #sleep(30)
        

    # Login with the provied username and password and return if it was successful or not.
   
    """
    Test to ensure we can create a user with an email box and send ourselves an email
    message that will be properly delivered to our mailbox. This tests multiple components
    together (nsauth and nscache, ldap, postfix, dovecot, etc.)
    """
    def testSendRoundtripEmail(self):
        options = '-q -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oPubkeyAuthentication=no'                                                                         
        timeout = 3
        host_name = socket.gethostname()
        ip = socket.gethostbyname(host_name)
        user = 'foo'
        password = 'bar'
        cmd = "/bin/true"
        ssh_cmd = 'ssh %s@%s %s "%s"' % (user, host_name, options, cmd)
        try:
            child = pexpect.spawn(ssh_cmd, timeout=timeout)
            child.expect(['[pP]assword: '])
            child.sendline(password)                                                                                                                                                   
            child.expect(pexpect.EOF)                                                                                                                                                  
            child.close()                                                                                                                                                              
        except pexpect.exceptions.TIMEOUT:
            pass

        # Give bruce time to ban.
        print("Waiting 40 seconds for bruce to ban...", flush=True)
        sleep(40)

        # Check log to see if we were banned.
        cmd = [
            "journalctl",
            "-u",
            "bruce-banner"
        ]
        proc = subprocess.run(cmd, capture_output=True, text=True)
        found = False
        for line in proc.stdout.split("\n"):
            if "Banning ssh on IP {0}".format(ip) in line:
                found = True
        self.assertTrue(found, "IP banned after 1 failed ssh logins.")
 
        # Email tests.
        username = self.testUser 
        goodPassword = self.testPass 
        badPassword = "not a good password"
        email_domain_name = self.testDomain
        email_local = self.testUser 
        email_address = email_local + '@' + email_domain_name
        email_subject = "Hi Friend: {0}".format(random.random())

        # Do an IMAP login with a good password. This should exonerate our IP address, preventing
        # us from being banned for any incorrect mail logins.
        self.plant.emailImapLogin(username, goodPassword)

        # Try to login via smtp 11 times with wrong password. Should not result in ban because our 
        # IP is exonerated.
        i = 0
        while i < 11: 
            print("Sending email", flush=True)
            self.plant.emailSend(username, badPassword, email_address, email_address, email_subject)
            i += 1

        # Give bruce time to ban.
        print("Waiting 60 seconds for bruce to ban...", flush=True)
        sleep(60)

        # Check log to see if we were banned.
        cmd = [
            "journalctl",
            "-u",
            "bruce-banner"
        ]
        proc = subprocess.run(cmd, capture_output=True, text=True)
        found = False
        for line in proc.stdout.split("\n"):
            if "Banning mail on IP {0}".format(ip) in line:
                found = True
        self.assertFalse(found, "IP not banned after 11 failed smtp logins because we are exonerated.")

