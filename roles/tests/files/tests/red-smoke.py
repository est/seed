import unittest
import greenhouse 
import os
import random, string

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 
    # List of item ids we have to cleanup
    itemIds = {} 
    redAdminUser = 'admin'
    redAdminPass = 'admin23'
    testEmail = "admin@myorg.dev"
    testFqdn = "myorg.dev"
    testCanonicalEmail = "admin@mail.mayfirst.dev"
    testUser = "admin"
    testPass = "admin23"
    testAliasFqdn = "www.myorg.dev"
    testPtrFqdn = "myorg.dev"
    testPtrIpv4 = "1.2.3.4"
    testIpv4 = "172.18.0.15"
    testIpv6 = "fc00::1"
    testText = "test-text-123"
    testMxServerName = "a.mx.mayfirst.dev"
    testMxDist = "10"
    testAliasServerName = "a.webproxy.mayfirst.dev"
    testSrvServerName = "im.mayfirst.dev"
    testSrvFqdn = "_xmpp._tcp.myorg.dev"
    testSrvPort = "5233"
    testSrvWeight = "5"
    testSrvDist = "10"
    testSshfpr = "9b8910e44952157492afafa164dea63c9ae4f6d1b2234cb8342b155ecfb2effd"
    testSshfpAlgorithm = "1"
    testSshfpType = "2"
    testDatabaseName = "testo"
    testDatabaseUser = "testo"
    testDatabasePassword = "testotesto1"
    testDatabasePasswordEncrypted = '*AC9316FB9D12D0ED66EA3BC4F86F648E71354F90'

    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.redUrl = "https://members.mayfirst.{0}/api.php".format(os.environ.get('GREENHOUSE_TLD', 'dev'))
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass

    def tearDown(self):
        self.deleteControlPanelItems()
        return 

    def deleteControlPanelItems(self):
        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertTrue(self.plant.redApi('item', 'delete', setProps, whereProps))

    def disableControlPanelItems(self):
        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertTrue(self.plant.redApi('item', 'disable', setProps, whereProps))

    def testControlPanelItems(self):
        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "mx",
           "dns_server_name": "a.mx.mayfirst.dev",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
           "dns_mx_verified": "1",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "dkim",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "a",
           "dns_ip": self.testIpv4,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "aaaa",
           "dns_ip": self.testIpv6,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testPtrFqdn,
           "dns_type": "ptr",
           "dns_ip": self.testPtrIpv4,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "txt",
           "dns_text": self.testText,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }

        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testSrvFqdn,
           "dns_type": "srv",
           "dns_server_name": self.testSrvServerName,
           "dns_ttl": "3600",
           "dns_port": self.testSrvPort,
           "dns_weight": self.testSrvWeight,
           "dns_dist": self.testSrvDist,
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "sshfp",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": self.testSshfpAlgorithm,
           "dns_sshfp_type": self.testSshfpType,
           "dns_sshfp_fpr": self.testSshfpr,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testAliasFqdn,
           "dns_type": "alias",
           "dns_server_name": self.testAliasServerName,
           "dns_ttl": "3600",
           "dns_dist": self.testMxDist,
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 38,
           "hosting_order_id": 1,
           "mailbox_login": self.testUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 38

        setProps = {
           "service_id": 2,
           "hosting_order_id": 1,
           "email_address": self.testEmail,
           "email_address_recipient": "{0}@mail.mayfirst.dev".format(self.testUser)
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 2

        # Web configuration.
        setProps = {
           "service_id": 7,
           "hosting_order_id": 1,
           "web_conf_domain_names": self.testFqdn,
           "web_conf_tls": 1,
           "web_conf_settings": "ProxyPass /proxy http://localhost:8000/",
           "web_conf_cache_type": "none"
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 7 

        setProps = {
            "service_id": 3,
            "hosting_order_id": 1,
            "server_access_login": self.redAdminUser,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 3 
       
        # Scheduled job.
        setProps = {
            "service_id": 24,
            "hosting_order_id": 1,
            "cron_schedule": "forever",
            "cron_cmd": "php -S localhost:8000",
            "cron_login": self.redAdminUser
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 24 

        # Database 
        setProps = {
            "service_id": 20,
            "hosting_order_id": 1,
            "mysql_db_name": self.testDatabaseName
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 20 

        # Database user
        setProps = {
            "service_id": 21,
            "hosting_order_id": 1,
            "mysql_user_name": self.testDatabaseUser,
            "mysql_user_db": self.testDatabaseName,
            "mysql_user_password": self.testDatabasePasswordEncrypted,
            "mysql_user_priv": "full"

        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 21 

        # Nextcloud user
        setProps = {
            "service_id": 37,
            "hosting_order_id": 1,
            "nextcloud_login": self.testUser,

        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsNot(itemId, False)
        self.itemIds[itemId] = 37

        # Now disable all of them
        self.disableControlPanelItems()
