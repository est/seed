import unittest
import greenhouse 
import os
import random, string
from time import sleep
import socket
import requests
from IPy import IP
import dns.resolver

class GreenhouseTestCase(unittest.TestCase):
    # Our top level domain will either be dev in the dev environment or
    # test in the test environment. The main domain will always be either
    # mayfirst.test or mayfirst.dev.
    plant = None
    environment = None 
    # List of item ids we have to cleanup
    itemIds = {} 
    redAdminUser = 'admin'
    redAdminPass = 'admin23'
    testFqdn = "myorg.dev"
    testAliasFqdn = "www.myorg.dev"
    testPtrFqdn = "myorg.dev"
    testPtrIpv4 = "1.2.3.4"
    testIpv4 = "172.18.0.15"
    testIpv6 = "fc00::1"
    testText = "test-text-123"
    testMxServerName = "a.mx.mayfirst.dev"
    testMxDist = "10"
    testAliasServerName = "a.webproxy.mayfirst.dev"
    testSrvServerName = "im.mayfirst.dev"
    testSrvFqdn = "_xmpp._tcp.myorg.dev"
    testSrvPort = "5233"
    testSrvWeight = "5"
    testSrvDist = "10"
    testSshfpr = "9b8910e44952157492afafa164dea63c9ae4f6d1b2234cb8342b155ecfb2effd"
    testSshfpAlgorithm = "1"
    testSshfpType = "2"

    def setUp(self):
        # Setup class variables in the test class.
        self.environment = os.environ.get('GREENHOUSE_ENVIRONMENT', 'dev')

        # Setup the plant object we will be using through the tests.
        self.plant = greenhouse.plant()
        self.plant.emailServer = 'mail.mayfirst.' + os.environ.get('GREENHOUSE_TLD', 'dev')
        self.plant.redUrl = "https://members.mayfirst.{0}/api.php".format(os.environ.get('GREENHOUSE_TLD', 'dev'))
        self.plant.redUser = self.redAdminUser
        self.plant.redPass = self.redAdminPass
        self.setupControlPanelItems()

    def tearDown(self):
        self.deleteControlPanelItems()
        return 

    def deleteControlPanelItems(self):
        for itemId in reversed(list(self.itemIds.keys())):
            serviceId = self.itemIds[itemId]
            whereProps = {
                "service_id": serviceId,
                "item_id": itemId
            }
            setProps = {}
            self.assertTrue(self.plant.redApi('item', 'delete', setProps, whereProps))


    # Add dns, email address via the control panel API.
    def setupControlPanelItems(self):
        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "a",
           "dns_ip": self.testIpv4,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "aaaa",
           "dns_ip": self.testIpv6,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testPtrFqdn,
           "dns_type": "ptr",
           "dns_ip": self.testPtrIpv4,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "txt",
           "dns_text": self.testText,
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }

        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "mx",
           "dns_server_name": self.testMxServerName,
           "dns_ttl": "3600",
           "dns_dist": self.testMxDist,
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testSrvFqdn,
           "dns_type": "srv",
           "dns_server_name": self.testSrvServerName,
           "dns_ttl": "3600",
           "dns_port": self.testSrvPort,
           "dns_weight": self.testSrvWeight,
           "dns_dist": self.testSrvDist,
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "dkim",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testFqdn,
           "dns_type": "sshfp",
           "dns_ttl": "3600",
           "dns_sshfp_algorithm": self.testSshfpAlgorithm,
           "dns_sshfp_type": self.testSshfpType,
           "dns_sshfp_fpr": self.testSshfpr,
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9

        setProps = {
           "service_id": 9,
           "hosting_order_id": 1,
           "dns_fqdn": self.testAliasFqdn,
           "dns_type": "alias",
           "dns_server_name": self.testAliasServerName,
           "dns_ttl": "3600",
           "dns_dist": self.testMxDist,
           "dns_sshfp_algorithm": "0",
           "dns_sshfp_type": "0",
        }
        itemId = self.plant.redApi('item', 'insert', setProps)
        self.assertIsInstance(itemId, int)
        self.itemIds[itemId] = 9


    """
    Test all DNS records. 
    """
    def testDnsRecords(self):
        print("Sleeping for 75 seconds to let DNS records propagate.")
        sleep(75)
        print("Done sleeping.")
        # Use our auth name servers to avoid caching issues.
        resolver = dns.resolver.Resolver(configure = False)
        resolver.nameservers = [ "172.18.0.11" ]
        for answer in resolver.resolve(self.testFqdn, 'A'):
            self.assertEqual(answer.to_text(), self.testIpv4)
        for answer in resolver.resolve(self.testFqdn, "AAAA"):
            self.assertEqual(answer.to_text(), self.testIpv6)
        # We have to do a reverse lookup of the IP address to check for pointer
        # records. This takes an IP like 1.2.3.4 and converts it to 
        # 4.3.2.1.in-addr.arpa.
        ip_obj = IP(self.testPtrIpv4)
        ip_reverse_name = ip_obj.reverseNames()[0]
        
        for answer in resolver.resolve(ip_reverse_name, 'PTR'):
            self.assertEqual(answer.to_text(), "{0}.".format(self.testPtrFqdn))
        for answer in resolver.resolve(self.testFqdn, "txt"):
            self.assertEqual(answer.to_text(), '"{0}"'.format(self.testText))
        for answer in resolver.resolve(self.testFqdn, "mx"):
            self.assertEqual(answer.to_text(), "{0} {1}.".format(self.testMxDist, self.testMxServerName))
        for answer in resolver.resolve(self.testSrvFqdn, "srv"):
            self.assertEqual(answer.to_text(), "{0} {1} {2} {3}.".format(self.testSrvDist, self.testSrvWeight, self.testSrvPort, self.testSrvServerName))
        for answer in resolver.resolve("mayfirst1._domainkey.{0}".format(self.testFqdn), "txt"):
            self.assertTrue(answer.to_text().startswith('"v=DKIM1'))
        for answer in resolver.resolve(self.testFqdn, "sshfp"):
            self.assertTrue(answer.to_text().startswith('1 2 '))
        # This effectively tests the alias record.
        for answer in resolver.resolve(self.testAliasFqdn, 'A'):
            self.assertEqual(answer.to_text(), self.testIpv4)

        return True
