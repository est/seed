---
# tasks file for nextcloud
- name: install base packages
  apt:
    name:
      - curl 
      - bzip2
      - python3
      - python3-requests
      - python3-packaging
      - lvm2
      - xfsprogs
      - postgresql-client
      - docker-compose

# All instances share the same docker, redis, and notifypush images, including all third
# party apps (even if they choose not to enable them all).
- name: Copy docker files 
  copy:
    src: "{{ item }}/"
    dest: /usr/local/src/{{ item }}
  loop:
    - docker-nextcloud
    - docker-redis
    - docker-notifypush
  tags: nextcloud-docker

- name: Ensure the nextcloud installer and entrypoint files are executable.
  file:
    mode: "0755"
    path: /usr/local/src/docker-nextcloud/{{ item }}
  loop:
    - nextcloud-download
    - docker-entrypoint.sh

- name: Ensure base nextcloud directories are there
  file:
    state: directory
    path: "{{ item }}"
  loop:
    - /var/www/nextcloud
    - /srv/nextcloud

# We have to install nextcloud on the host as well as the docker image, so nginx
# can server static files.
- name: install all needed versions of nextcloud on the host
  command: /usr/local/src/docker-nextcloud/nextcloud-download
  args:
    creates: /var/www/nextcloud/{{ item.version }}
  environment:
    NEXTCLOUD_VERSION: "{{ item.version }}"
  loop: "{{ nextcloud }}"

# We don't actually need users and groups for each instance on the host, but it
# makes it more intelligible when checking permissions to see usernames and group
# names when viewing and setting permissions.
- name: create host user for each instance
  user:
    name: "{{ item.user }}"
    home: /srv/nextcloud/{{ item.user }}
    state: present
    uid: "{{ item.uid }}"
  loop: "{{ nextcloud }}"

- name: ensure group has same gid as user
  group:
    name: "{{ item.user }}"
    gid: "{{ item.uid }}"
  loop: "{{ nextcloud }}"

- name: ensure the user owns their home directory
  file:
    path: /srv/nextcloud/{{ item.user }}
    state: directory
    owner: "{{ item.user }}"
  loop: "{{ nextcloud }}"

# NOTE: we simply re-use the user variable here.
- name: create host group for each instance. 
  group:
    name: "{{ item.user }}"
    state: present
  loop: "{{ nextcloud }}"

# Each instances has a directory in /srv which holds it's unique data directory
# and config directory.
- name: create data directory for each instance with proper permissions
  file:
    state: directory
    path: /srv/nextcloud/{{ item.user }}/data
    owner: "{{ item.user }}"
    group: "{{ item.user }}"
  loop: "{{ nextcloud }}"

- name: create config directory for each instance with proper permissions
  file:
    state: directory
    path: /srv/nextcloud/{{ item.user }}/config
    owner: "{{ item.user }}"
    group: "{{ item.user }}"
  loop: "{{ nextcloud }}"

- name: create php sessions directory for each instance with proper permissions
  file:
    state: directory
    path: /srv/nextcloud/{{ item.user }}/sessions
    owner: "{{ item.user }}"
    group: "{{ item.user }}"
  loop: "{{ nextcloud }}"

- name: create docker-compose yaml files for each instance
  template:
    src: docker-compose.yml.j2
    dest: /srv/nextcloud/{{ item.user }}/docker-compose.yml
  loop: "{{ nextcloud }}"
  tags:
    - nextcloud-upgrade

- name: create an env file with the instance variables for each instance. 
  template:
    src: env.j2
    dest: /srv/nextcloud/{{ item.user }}/config/env
  loop: "{{ nextcloud }}"

- name: create nginx config file for each instance, notify nginx reload
  template:
    src: nextcloud.conf.j2
    dest: /etc/nginx/sites-available/{{ item.user }}.conf
  loop: "{{ nextcloud }}"
  notify: reload nginx
  tags:
    - nextcloud-upgrade

- name: create symlink for each instance, notify nginx reload
  file:
    state: link
    src: /etc/nginx/sites-available/{{ item.user }}.conf
    dest: /etc/nginx/sites-enabled/{{ item.user }}.conf
  loop: "{{ nextcloud }}"
  notify: reload nginx

- name: add nextcloud helper files
  copy:
    src: "{{ item }}" 
    dest: /usr/local/sbin/{{ item }}
    mode: "0755"
  loop:
    - mf-nextcloud-install
    - mf-nextcloud-purge
    - mf-nextcloud-occ-wrapper
    - mf-nextcloud-backup-database
    - mf-phpsessionclean

- name: add nextcloud systemd cron service
  template:
    src: nextcloud-cron.service.j2
    dest: /etc/systemd/system/{{ item.user }}-nextcloud-cron.service
  loop: "{{ nextcloud }}"
  tags:
    - nextcloud-upgrade
  notify:
    - systemctl daemon-reload

- name: add nextcloud systemd cron timer 
  template:
    src: nextcloud-cron.timer.j2
    dest: /etc/systemd/system/{{ item.user }}-nextcloud-cron.timer
  loop: "{{ nextcloud }}"

- name: add nextcloud php session cleaning systemd files
  copy:
    dest: /etc/systemd/system/{{ item }}
    src: "{{ item }}"
  loop:
    - mf-phpsessionclean.service
    - mf-phpsessionclean.timer
  notify:
    - systemctl daemon-reload
    - systemctl enable phpsession timer
    - systemctl start phpsession timer


