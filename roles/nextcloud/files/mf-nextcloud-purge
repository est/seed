#!/bin/bash

# Script to purge a nextcloud instance. Pass $USER as first argument.

function die() {
  printf "$1\n" "$2"
  exit 1
}

user="$1"
[ -z "$user" ] && die "Please pass the user as the first argument."

base="/srv/nextcloud/$user"
[ ! -d "$base" ] && die "Can't find base directory (%s)." "$base"

env="${base}/config/env"
[ ! -f "$env" ] && die "Can't locate the env file (%s)." "$env"

database_host=$(grep ^DATABASE_HOST "${base}/config/env" | cut -d= -f2)
hostname=$(grep ^HOSTNAME "${base}/config/env" | cut -d= -f2)

database_host=10.25.17.9
printf "Dropping database %s on host %s.\n" "$user" "$database_host"
ssh "postgres@${database_host}" "psql -c 'DROP DATABASE IF EXISTS $user'"

printf "Deleting user %s.\n" "$user"
ssh "postgres@${database_host}" "dropuser $user"

printf "Stopping and Deleting docker containers %s.\n" "$user"
docker stop "${user}-nextcloud"
docker rm "${user}-nextcloud"
docker stop "${user}-redis"
docker rm "${user}-redis"

nginx_enabled="/etc/nginx/sites-enabled/${user}.conf"
nginx_available="/etc/nginx/sites-available/${user}.conf"

printf "Removing nginx files.\n"
rm -f "$nginx_enabled" "$nginx_available"

printf "Remove x509 certificate.\n"
rm -rf /etc/ssl/mayfirst/${hostname}

printf "Unmounting partition.\n"
umount "$base"

printf "Deleting directory %s\n" "$base"
rm -rf "$base"

printf "Removing line from /etc/fstab\n"
# Note: the \# business sets up # as the delimiter instead of the typical
# forward slash delimiter.
sed -i "\#$base#d" /etc/fstab

printf "Removing systemd service and timer files.\n"
rm -f "/etc/systemd/system/${user}-nextcloud-cron.timer" "/etc/systemd/system/${user}-nextcloud-cron.service"

printf "Removing logical volume.\n"
lvremove "vg0/$user"
